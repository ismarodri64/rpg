﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats
{
    // Clase de stats

    public static int hp = 100;
    public static int maxHp = 100;
    public static int mana = 100;
    public static int maxMana = 100;
    public static int gold = 0;
    public static int xp = 0;
    public static int xpToNextLevel = 100;
    public static int stamina = 5;
    public static int iniciativa = 3;

}
