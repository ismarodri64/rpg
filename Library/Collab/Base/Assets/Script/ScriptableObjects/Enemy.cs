﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Enemy : ScriptableObject
{

    public static string enemyName;
    public int health = 100;
    public int mana = 100;
    
}
