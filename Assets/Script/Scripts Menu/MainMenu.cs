﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //Cargamos las opciones que nos sale en el menu y que nos lleva a su respectiva pantalla
    public void EscenaJuego()
    {
        SceneManager.LoadScene("Overworld_1");
    }

    public void CargarOpcion(string siguienteCuadro)
    {
        SceneManager.LoadScene(siguienteCuadro);
    }
    //Saldremos del juego al darle a esta opcion
    public void Salir()
    {
        Application.Quit();
    }
}
