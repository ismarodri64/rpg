﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverWorldSpawner : MonoBehaviour
{
    int enemyNumber;

    // Start is called before the first frame update
    void Start()
    {
        enemyNumber = Random.Range(10, 20);
        for (int i = 0; i < enemyNumber; i++)
        {
            int type = Random.Range(0, 4);

            switch (type)
            {
                case 0:
                    Instantiate(Resources.Load("Enemigos Prefabs/BossAraña"),new Vector2(Random.Range(-20, 20), Random.Range(-20, -1)) ,Quaternion.identity);
                    break;
                case 1:
                    Instantiate(Resources.Load("Enemigos Prefabs/BossCalamar"), new Vector2(Random.Range(2, 40), Random.Range(-6, 6)), Quaternion.identity);
                    break;                   
                case 2:
                    Instantiate(Resources.Load("Enemigos Prefabs/BossDino"), new Vector2(Random.Range(-9, 2), Random.Range(5, 25)), Quaternion.identity);
                    break;
                case 3:
                    Instantiate(Resources.Load("Enemigos Prefabs/BossRata"), new Vector2(Random.Range(-40, -7), Random.Range(-9, 13)), Quaternion.identity);
                    break;
            }
        }
    }

}
