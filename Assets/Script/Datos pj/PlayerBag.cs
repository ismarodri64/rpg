﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBag : MonoBehaviour
{

     public static ArrayList bag = new ArrayList();
     public static Dictionary<string, int> bagValues = new Dictionary<string, int>();


      public static void AddToBag(ScriptableObject a) {
          bag.Add(a);
          CountItems();
      }
      public static void RemoveFromBag(ScriptableObject a)
      {
          int pointer = 0;
          foreach(Item i in bag)
          {
              if(a.name == i.name)
              {
                  bag.RemoveAt(pointer);
                  break;
              }
              pointer++;
          }
          CountItems();
      }
      static void CountItems()
      {
          int granada = 0;
          int pocion = 0;
          int multipot = 0;

          foreach(Item a in bag)
          {
              switch (a.name)
              {
                  case "granada":
                      granada++;
                      break;
                  case "pocion":
                      pocion++;
                      break;
                  case "MultiPocion":
                      multipot++;
                      break;
              }  
          }

          if (!bagValues.ContainsKey("Granada"))
          {
              bagValues.Add("Granada", granada);
          }
          if (!bagValues.ContainsKey("Pocion"))
          {
              bagValues.Add("Pocion", pocion);
          }
          if (!bagValues.ContainsKey("MultiPocion"))
          {
              bagValues.Add("MultiPocion", multipot);
          }

          bagValues["Granada"] = granada;
          bagValues["Pocion"] = pocion;
          bagValues["MultiPocion"] = multipot;

      }
    
      public static void LoadItemsFromSave(ArrayList a)
      {
          foreach(Item i in a)
          {
              AddToBag(i);
          }
      }
    
      public static void SaveGameBag()
      {
          BagData dat = new BagData(bag, bagValues);
          string jsn = JsonUtility.ToJson(dat);
          System.IO.File.WriteAllText(Application.persistentDataPath + "/SaveDataBag.json", jsn);
      }

      public static void LoadGame()
      {
          if (System.IO.File.Exists(Application.persistentDataPath + "/SaveDataBag.json"))
          {
              string jsn = System.IO.File.ReadAllText(Application.persistentDataPath + "/SaveDataBag.json");
              BagData dat = JsonUtility.FromJson<BagData>(jsn);
              LoadItemsFromSave(dat.bag);
          }
          }
    
}
