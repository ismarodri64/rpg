﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

[Serializable]
public class Stats
{
    // Clase de stats

    public static int hp = 100;
    public static int maxHp = 100;
    public static int mana = 100;
    public static int maxMana = 100;
    public static int gold = 0;
    public static int xp = 0;
    public static int xpToNextLevel = 70;
    public static int stamina = 5;
    public static int iniciativa = 3;
    public static int lvl = 1;

    public static void SaveData()
    {
        DataObject saveData = new DataObject(hp, maxHp, mana, maxMana, gold, xp, xpToNextLevel, stamina, iniciativa, lvl);
        string jsn = JsonUtility.ToJson(saveData);
        System.IO.File.WriteAllText(Application.persistentDataPath + "/SaveData.json", jsn);
    }

    public static void LoadData()
    {
        if (System.IO.File.Exists(Application.persistentDataPath + "/SaveData.json")){
            string jsn = System.IO.File.ReadAllText(Application.persistentDataPath + "/SaveData.json");
            DataObject loadedData = JsonUtility.FromJson<DataObject>(jsn);
            hp = loadedData.hp;
            maxHp = loadedData.hp;
            mana = loadedData.mana;
            maxMana = loadedData.maxMana;
            gold = loadedData.gold;
            xp = loadedData.xp;
            xpToNextLevel = loadedData.xpToNextLevel;
            stamina = loadedData.stamina;
            iniciativa = loadedData.iniciativa;
            lvl = loadedData.lvl;
            SceneManager.LoadScene("Overworld_1");
        }     

    }

}

