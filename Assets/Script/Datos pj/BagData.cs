﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagData
{
    public ArrayList bag = new ArrayList();
    public Dictionary<string, int> bagValues = new Dictionary<string, int>();

    public BagData(ArrayList bag, Dictionary<string, int> bagValues)
    {
        this.bag = bag;
        this.bagValues = bagValues;
    }

}
