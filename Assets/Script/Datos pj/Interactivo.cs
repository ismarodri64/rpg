﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Interactivo : MonoBehaviour,IPointerDownHandler
{

    protected BoxCollider2D colisionador;
    public UnityEvent OnInteraccion;


    // Start is called before the first frame update
    private void Start()
    {
        colisionador = GetComponent<BoxCollider2D>();
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnInteraccion.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Haciendo click");
    }
}
