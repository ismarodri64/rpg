﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackPanelTargets : MonoBehaviour
{
    public delegate void choice();
    public static event choice OnAttack;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<Button>().onClick.AddListener(PlayerCombatChoice);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayerCombatChoice()
    {
        if(OnAttack != null)
        {
            OnAttack();
            Debug.Log("Evento de attack");
        }
    }

}
