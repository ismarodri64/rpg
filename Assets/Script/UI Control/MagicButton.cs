﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MagicButton : MonoBehaviour
{
    GameObject magicPanel;
    GameObject attackPanel;
    GameObject itemPanel;

    // Start is called before the first frame update
    void Start()
    {
        magicPanel = GameObject.Find("MagicPanel");      
        this.gameObject.GetComponent<Button>().onClick.AddListener(SpawnMenu);
        itemPanel = GameObject.Find("ItemPanel");
        attackPanel = GameObject.Find("AttackPanel");
        magicPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnMenu()
    {

        if (CombatLogic.usableUI)
        {
            if (magicPanel.activeSelf)
            {
                magicPanel.SetActive(false);
            }
            else
            {
                magicPanel.SetActive(true);
            }

        }

    }
}
