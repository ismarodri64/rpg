﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackButton : MonoBehaviour
{
    GameObject magicPanel;
    GameObject attackPanel;
    GameObject itemPanel;

    // Start is called before the first frame update
    void Start()
    {
        attackPanel = GameObject.Find("AttackPanel");
        attackPanel.SetActive(false);
        this.gameObject.GetComponent<Button>().onClick.AddListener(SpawnMenu);
        itemPanel = GameObject.Find("ItemPanel");
        magicPanel = GameObject.Find("MagicPanel");
    }

    void SpawnMenu()
    {

        if (CombatLogic.usableUI)
        {
            if (attackPanel.activeSelf)
            {
                attackPanel.SetActive(false);
            }
            else
            {
                attackPanel.SetActive(true);
            }

        }

    }
}
