﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSystem : MonoBehaviour
{
    public Stats stats;

    // Elementos UI
    public Text Level_Text;
    public Text Exp_Text;
    public Text NextLevel_Text;

    void Start()
    {
        Stats.xpToNextLevel = 50;
    }

    void Update()
    {

        Level_Text.text = "Lvl: " + Stats.lvl;
        Exp_Text.text = "Exp: " + Stats.xp;
        NextLevel_Text.text = "NextLevel: " + Stats.xpToNextLevel;

        if (Input.GetKeyDown(KeyCode.F))
        {
            GetExp();
        }
    }

    public void GetExp()
    {
        Stats.xp += 50;

        if(Stats.xp >= Stats.xpToNextLevel)
        {
            Stats.lvl += 1;
            Stats.xpToNextLevel += 20;
            Stats.xp = 0;
        }
    }
}
