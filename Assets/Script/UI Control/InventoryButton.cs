﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryButton : MonoBehaviour
{
    GameObject magicPanel;
    GameObject attackPanel;
    GameObject itemPanel;
    // Start is called before the first frame update
    void Start()
    {
        itemPanel = GameObject.Find("ItemPanel");
        itemPanel.SetActive(false);
        this.gameObject.GetComponent<Button>().onClick.AddListener(SpawnMenu);
        magicPanel = GameObject.Find("MagicPanel");
        attackPanel = GameObject.Find("AttackPanel");
    }

    void SpawnMenu()
    {
        if (CombatLogic.usableUI)
        {
            if (!itemPanel.activeSelf)
            {
                itemPanel.SetActive(true);
            }
            else
            {
                itemPanel.SetActive(false);
            }

            

        }
        

    }

}
