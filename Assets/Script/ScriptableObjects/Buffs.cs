﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Buffs : ScriptableObject
{
    public int potency;
}
