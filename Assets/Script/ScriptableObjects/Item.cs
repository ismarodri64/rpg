﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Item : ScriptableObject
{
    /* Start is called before the first frame update
    public int potency;
    //"Ally" para aliado "Enemy" para enemigos
    public string target;
    public bool multiTarget;
    public bool healing;
    */

    // Seran los datos del item que tengamos 
    public Sprite sprite;
    public string nombre;
    public bool apilable;
    [TextArea(1, 3)]//definimos el numero max y min de lineas de nuestro text
    public string descripcion;


    public virtual bool UsarItem()
    {
        Debug.Log("Usando " + nombre);
        return true;
    }

}
