﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMono : MonoBehaviour
{
    //Cada enemigo tendra su scriptable.
    public Enemy enScriptable;
    public int life;
    //Si, otra vez he hardcodeado
    GameObject canvashard;

    // Start is called before the first frame update
    void Start()
    {
        canvashard = GameObject.Find("Canvas");
        //Debug.Log(canvashard.name);
        life = enScriptable.health;
    }

    // Update is called once per frame
    void Update()
    {
        lifebar();
        //Debug.Log(this.gameObject.name+" "+life);
        if (life <= 0)
        {
            
            Debug.Log(this.gameObject.name + " dead");
            canvashard.GetComponent<LevelSystem>().GetExp();
            Destroy(gameObject);
        }
    }


    private void lifebar()
    {
        //Lo que hago es pasar los 2 ints a float (lo hago de esta forma para ahorrar anchura en la linea del localscale y no liarme), y los divido entre ellos, si no han quitado vida, sera 1 el resultado porque la vida dividido entre la vida maxima del scriptable, seria 1, y si baja, seria 0.9, 0.8...
        float lifef = (float)life;
        float lifeScriptable = (float)enScriptable.health;
        this.GetComponent<Transform>().GetChild(0).transform.localScale = new Vector2(lifef / lifeScriptable, this.GetComponent<Transform>().GetChild(0).transform.localScale.y);
    }


    public void LifeDown(int a)
    {
        Debug.Log("VIDA ENEMIGA RESTADA " + a);
        this.life -= a;
    }
}
