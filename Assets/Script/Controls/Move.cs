﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class Move : MonoBehaviour
{
    bool noaccess= false;

    //Camera camera;
    //Lol, es mas facil de lo que me esperaba xd.
    int movecells = Stats.stamina;
    GameObject ground;
    Tilemap groundtm;
    Transform positioncharacter;
    //Si esta true, estas en modo lucha y se le asignara un limite de velocidad
    bool fightmode = false;
    // Start is called before the first frame update
    void Start()
    {
        ground = GameObject.Find("Ground");
        groundtm= ground.GetComponent<Tilemap>();
        positioncharacter = this.gameObject.transform;
        //La escena combat es la unica que estaras en modo lucha.
        if (SceneManager.GetActiveScene().name == "Combat")
        {
            fightmode = true;
        }
        else
        {
            //camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            fightmode = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        //La camara tiene que ser vector 3 (-10 es default)
        if (Input.GetMouseButtonDown(1))
        {
            StartCoroutine(gettheposition());
            //centerposition();
            centerposition2();
        }

        //Debug.Log(Input.mousePosition);
        //0 click izquierdo, 1 click derecho, 2 click en medio
        
    }

    private void centerposition2()
    {
        //Coge la posicion del mouse, luego el gridpos pasa la posicion del mouse a la celda, y si el mouse clica en una celda, dentro pasa la posicion de la celda al mundo (si no pasas de celda a mundo, se desposiciona).
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //Vector2 playerVector = new Vector2(positioncharacter.position.x, positioncharacter.position.y);
        Vector3Int gridPosplayer = groundtm.WorldToCell(this.transform.position);
        
        Vector3Int gridPos = groundtm.WorldToCell(mousePos);
        if (groundtm.HasTile(gridPos))
        {
            //Debug.Log(groundtm.GetTile(gridPos).name);
            //Debug.Log("Player " + gridPosplayer);
            //Debug.Log("Mouse "+gridPos);
            //Debug.Log(mousePos);
            Vector2 vectorposition= groundtm.CellToWorld(gridPos);
            if (fightmode)
            {
                limitCells(gridPosplayer, gridPos);
            }
            else
            {
                positioncharacter.transform.position = vectorposition;
                //Debug.Log(this.transform.position);
            }
            //positioncharacter.transform.position
            //new Vector2(gridPos.x, gridPos.y);
        }
    }

    private void limitCells(Vector3Int playerg, Vector3Int mouseg)
    {


        if (
               mouseg.x > playerg.x + movecells
            || mouseg.y > playerg.y + movecells
            || mouseg.x < playerg.x - movecells
            || mouseg.y < playerg.y - movecells
           )
        {
            Debug.Log("You can't go mor than " + movecells + " cells");
        }
        else
        {
            positioncharacter.transform.position = groundtm.CellToWorld(mouseg);
        }

    }

    //Lo que hago aqui es guardar la posicion del jugador antes de que el jugador se mueva, pasan 0.1 segundos que es cuando ya ha hecho el transform del metodo centerposition,
    //y si el jugador ya ha colisionado con algo que es el noground o obstacle (esta en el oncollisionenter), el noaccess se pone a true y el jugador se posicionara en la posicion anterior
    //y asi impedimos que se descoloque de las casillas.
    IEnumerator gettheposition()
    {
        float positionx = positioncharacter.position.x;
        float positiony = positioncharacter.position.y;
        //Nota: con wait for end frame no va bien.
        yield return new WaitForSeconds(0.1f);
        if (noaccess)
        {
            noaccess = false;
            positioncharacter.position = new Vector2(positionx, positiony);

        }
    }

    
    private void OnCollisionEnter2D(Collision2D collision)
    {

        //Si collisionan con Obstacles o notground, noaccess es true
        //Debug.Log(collision.gameObject.name);
        if (collision.gameObject.name== "Obstacles" || collision.gameObject.name == "NotGround")
        {

            noaccess = true;
        }
        
    }

}
